import {UserInterface} from './user.interface';

export interface ApiResponseInterface {
    // "page":1,"per_page":6,"total":12,"total_pages":2,"data":
    page: number;
    per_page: number;
    total: number;
    total_pages: number;
    data: UserInterface[];
}
