import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {UserInterface} from '../interfaces/user.interface';
import {Observable} from 'rxjs';
import {HttpDataServiceService} from '../services/http-data-service.service';
import {CachingService} from '../services/caching.service';

const STATIC_EXAMPLE_BY_HTTP_URL: string = 'https://reqres.in/api/users/';

@Injectable({
  providedIn: 'root'
})
export class UsersResolverService implements Resolve<{ data : UserInterface }>{

  constructor(
      private httpDataServiceService: HttpDataServiceService,
      private cachingService: CachingService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<{ data: UserInterface }> | { data: UserInterface } {
    const id = route.params.id;
    const item = this.cachingService.loadCachedData(id);
    if (item) {
      return {data : item.data as UserInterface}
    }
    return this.httpDataServiceService.get<{data: UserInterface}>(STATIC_EXAMPLE_BY_HTTP_URL + id,)
  }
}
