import {Component, OnInit, ViewChild} from '@angular/core';
import {NgProgressComponent} from 'ngx-progressbar';
import {NavigationEnd, NavigationStart, Router,} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'mzh-quizz-task';
  @ViewChild('progressBar', {static: true}) progressBar : NgProgressComponent;

  constructor(private router: Router) {
  }
  ngOnInit(): void {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart){
        this.progressBar.start();
      }
      if (event instanceof NavigationEnd){
        this.progressBar.complete();
      }
    })
  }


}
