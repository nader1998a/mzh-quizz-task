import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MainComponent} from './components/main/main.component';
import {UserDetailsComponent} from './components/user-details/user-details.component';
import {UsersResolverService} from './resolvers/users-resolver.service';


const routes: Routes = [
  { path: '', component: MainComponent },
  { path: 'users/:id', component: UserDetailsComponent, resolve: {userDetails: UsersResolverService} },
  { path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
