import {EventEmitter, Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CachingService} from './caching.service';


export interface RequestCachingOptions {
  userId: number;
  key: string;
  data: any;
  expireTime: number; // in Minutes
}


@Injectable({
  providedIn: 'root'
})
export class HttpDataServiceService {

  public httpEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private http: HttpClient,
              private cache: CachingService) { }

  public get<T>(url: string, options?: RequestCachingOptions, headers?: HttpHeaders) : Observable<T> {
    return this.http.get<T>(url, {headers: headers});
  }

  public post<T>(url: string, payload: Object, options:any, headers?: HttpHeaders) : Observable<T> {
    return this.http.post<T>(url,payload,{headers: headers});
  }

}
