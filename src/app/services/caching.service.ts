import { Injectable } from '@angular/core';
import {RequestCachingOptions} from './http-data-service.service';

@Injectable({
  providedIn: 'root'
})
export class CachingService {

  constructor() { }

  setCachingData(cache: RequestCachingOptions) : void {
    const expireConfig = cache.expireTime || 0;
    const dataToCache = {
      data : cache.data,
      expire : new Date().getTime() + expireConfig * 60 * 1000,
    }
    localStorage.setItem(cache.key, JSON.stringify(dataToCache));
  }

  loadCachedData(key: string) : any {
    let item = localStorage.getItem(key);
    if (item){
      let parsedData = JSON.parse(item);
      // Check Expire Time
      if (parsedData.expire > new Date().getTime() ){
        return parsedData;
      } else {
        this.deleteExpireItem(key);
        return null;
      }
    } else
      return null;
  }

  clearCache(): void {
    localStorage.clear();
  }

  deleteExpireItem(id: string) : void {
    localStorage.removeItem('id');
  }
}
