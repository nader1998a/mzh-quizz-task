import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainComponent } from './components/main/main.component';
import {MaterialModule} from './modules/material/material.module';
import { UserCardComponent } from './components/user-card/user-card.component';
import {HttpClientModule} from '@angular/common/http';
import { UserDetailsComponent } from './components/user-details/user-details.component';
import {ReactiveFormsModule} from '@angular/forms';
import {NgProgressModule} from 'ngx-progressbar';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    UserCardComponent,
    UserDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule,
    ReactiveFormsModule,
    NgProgressModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
