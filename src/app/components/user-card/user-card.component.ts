import {Component, Input, OnInit} from '@angular/core';
import {UserInterface} from '../../interfaces/user.interface';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.scss']
})
export class UserCardComponent implements OnInit {

  private _user: UserInterface;

  public get user(): UserInterface {
    return this._user;
  }

  @Input() public set user(value: UserInterface) {
    this._user = value;
  }

  constructor(
      private router: Router
  ) { }

  ngOnInit(): void {
  }

  getUserInfo(user: UserInterface): void {
    this.router.navigate(['./users', user.id]);
  }

  backToUsersList(): void {
    this.router.navigate(['']);
  }

  isUserDetailsView() : boolean {
    return this.router.url.includes('/users');
  }
}
