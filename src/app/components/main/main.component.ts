import {Component, OnInit, ViewChild} from '@angular/core';
import {HttpDataServiceService,} from '../../services/http-data-service.service';
import {UserInterface} from '../../interfaces/user.interface';
import {ApiResponseInterface} from '../../interfaces/apiResponse.interface';
import {FormControl} from '@angular/forms';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {MatPaginator, PageEvent} from '@angular/material/paginator';

const STATIC_EXAMPLE_HTTP_URL: string = "https://reqres.in/api/users";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  private _allUsers: UserInterface[] = [ ];

  get allUsers(): UserInterface[] {
    return this._allUsers;
  }

  set allUsers(value: UserInterface[]) {
    this._allUsers = value;
  }

  isPaginationRunning: boolean = false;

  currentUsers: UserInterface[] = [ ];
  total_pages: number;

  usersFilterFormControl: FormControl = new FormControl('',);

  @ViewChild('pagination', {static: true}) pagination: MatPaginator;

  constructor(
      private httpDataService: HttpDataServiceService
  ) { }

  ngOnInit(): void {
    this.getAvailableUsers(1);
    this.subscribeToFilterChanges();

    this.pagination.pageSizeOptions = [6, 12];
  }

  subscribeToFilterChanges(): void {
    this.usersFilterFormControl.valueChanges.pipe(
        distinctUntilChanged(),
        debounceTime(500)
    )
        .subscribe(id => {
          console.log(id);
          if (id && id != '')
            this.getUsersByIdFilter(id);
          else this.currentUsers = this.allUsers;
        })
  }

  getAvailableUsers(page: number): void {
    this.isPaginationRunning = true;
    let getUrl: string = `${STATIC_EXAMPLE_HTTP_URL}?page=${page}`;
    this.httpDataService.get<ApiResponseInterface>(getUrl,).subscribe(res => {
      this.allUsers = res.data;
      this.currentUsers = res.data;
      this.pagination.length = res.total;
      this.pagination.pageSize = res.per_page;
    }, error => {
      console.log(error)
    },() => {
      this.isPaginationRunning = false;
    });
  }

  getUsersByIdFilter(id): void {
    this.currentUsers = this.allUsers.filter(user => {
      return user.id.toString() === id.toString()
    })
  }

  pageChanged(page: PageEvent) : void {
    console.log(page);
    if (page.pageIndex == 0 || page.pageIndex == 1)
      this.getAvailableUsers(++page.pageIndex);
  }
}
