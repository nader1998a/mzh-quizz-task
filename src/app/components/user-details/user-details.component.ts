import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {UserInterface} from '../../interfaces/user.interface';
import {HttpDataServiceService} from '../../services/http-data-service.service';
import {CachingService} from '../../services/caching.service';


const STATIC_EXAMPLE_BY_HTTP_URL: string = 'https://reqres.in/api/users/';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {

  user: UserInterface;

  constructor(
      private route: ActivatedRoute,
      private httpDataServiceService: HttpDataServiceService,
      private cachingService: CachingService,
  ) {
    this.user = {avatar: '', email: '', first_name: '', id: 0, last_name: ''};
  }

  ngOnInit(): void {
    this.route.data.subscribe(res => {
      this.user = res.userDetails.data;
      // Check user cache if existed and valid before updating cache
      if (!this.cachingService.loadCachedData(this.user.id.toString())){
        this.cachingService.setCachingData({
          data: this.user, expireTime: 1, key: this.user.id.toString(), userId: this.user.id
        })
      }
    })
    // To add dynamic param update if needed;
    this.subscribeToUserId();
  }

  subscribeToUserId(): void {
    this.route.params.subscribe(value => {
      if (!this.user)
        this.getUserById(value['id']);
    })
  }

  getUserById(id) : void {
    const item = this.cachingService.loadCachedData(id) ;
    if (item){
      this.user = item.data as UserInterface;
      return;
    }
    this.httpDataServiceService.get<{data: UserInterface}>(STATIC_EXAMPLE_BY_HTTP_URL + id,)
        .subscribe(res => {
            console.log(res);
            this.user = res.data;
          if (!this.cachingService.loadCachedData(this.user.id.toString())){
            this.cachingService.setCachingData({
              data: this.user, expireTime: 1, key: this.user.id.toString(), userId: this.user.id
            })
          }
        })
  }

}
